

def TowerOfHanoi(n, source, destination, aux):
    if n == 1:
        print("Move disk 1 from rod", source, "to rod", destination)
        return
    TowerOfHanoi(n - 1, source, aux, destination)
    print("Move disk", n, "from rod", source, "to rod", destination)
    TowerOfHanoi(n - 1, aux, destination, source)


n = int(input("Enter the number of disks"))
TowerOfHanoi(n, 'A', 'C', 'B')

